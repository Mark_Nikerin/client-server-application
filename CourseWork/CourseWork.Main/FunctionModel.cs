﻿using System;
using System.Collections.Generic;

namespace CourseWork.Main
{
    public interface IFunction
    {
        List<Tuple<double, double>> Points { get; set; }
    }
    public class RKFunction : IFunction
    {
        public int N { get; set; }
        public int T { get; set; }
        public double H { get; set; }
        public List<Tuple<double, double>> Points { get; set; }
        public RKFunction(int n, int t)
        {
            Points = new List<Tuple<double, double>>();
            N = n;
            T = t;
            H = (double) T / N;
        }
    }

    public class Function : IFunction
    {
        public int T;
        public List<Tuple<double, double>> Points { get; set; }

        public Function(int t)
        {
            Points = new List<Tuple<double, double>>();
            T = t;
        }
    }

}
