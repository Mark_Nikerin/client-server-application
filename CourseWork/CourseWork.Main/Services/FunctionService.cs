﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseWork.Main.Services
{
    public interface IFunctionService
    {
        double GetY(double x);
    }

    public class FunctionService : IFunctionService
    {  
        public double GetY(double x)
        {
            var xsqr = Math.Pow(x, 2);
            var cosx = Math.Cos(x);

            return (xsqr * cosx * Math.Pow(Math.E, cosx - 1)) / (2 + xsqr);
        }
    }
}
