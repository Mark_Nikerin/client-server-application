﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CourseWork.Main.Services
{
    public interface IFunctionHandlerService
    {
        List<Tuple<double, double>> GetFunctionPoints();
        List<Tuple<double, double>> GetFirstRungeKuttaPoints();
        List<Tuple<double, double>> GetSecondRungeKuttaPoints();
        List<Tuple<double, double>> GetLocalErrorPoints();
        double GetGlobalError();
        void SetParams(int n, int t);
    }


    public class FunctionHandlerService : IFunctionHandlerService
    {
        private readonly IFunctionService _f;

        private static Function _function;
        private static Function _error;

        private static RKFunction _rk1;
        private static RKFunction _rk2;

        public FunctionHandlerService(IFunctionService functionService)
        {
            _f = functionService;
        }

        public void SetParams(int n, int t)
        {
            _rk1 = new RKFunction(n, t);
            _rk2 = new RKFunction(2 * n, t);
            _function = new Function(t);
            _error = new Function(t);
        }

        public List<Tuple<double, double>> GetFirstRungeKuttaPoints()
        {
            double tnext = 0, ynext = 0.5, k1, k2, k3, k4;

            if (_rk1.Points.Count == 0)
            {
                for (int n = 0; n < _rk1.N; n++)
                {
                    k1 = _rk1.H * _f.GetY(ynext);
                    k2 = _rk1.H * _f.GetY(ynext + 0.5 * k1);
                    k3 = _rk1.H * _f.GetY(ynext + 0.5 * k2);
                    k4 = _rk1.H * _f.GetY(ynext + k3);
                    ynext += (k1 + 2 * k2 + 2 * k3 + k4) / 6;

                    _rk1.Points.Add(new Tuple<double, double>(tnext, ynext));

                    tnext +=_rk1.H;
                }
            } 
            return _rk1.Points;
        }

        public List<Tuple<double, double>> GetSecondRungeKuttaPoints()
        {
            double tnext = 0, ynext = 0.5, k1, k2, k3, k4;

            if (_rk2.Points.Count == 0)
            {
                for (int n = 0; n < _rk2.N; n++)
                {
                    k1 = _rk2.H * _f.GetY(ynext);
                    k2 = _rk2.H * _f.GetY(ynext + 0.5 * k1);
                    k3 = _rk2.H * _f.GetY(ynext + 0.5 * k2);
                    k4 = _rk2.H * _f.GetY(ynext + k3);
                    ynext += (k1 + 2 * k2 + 2 * k3 + k4) / 6;

                    _rk2.Points.Add(new Tuple<double, double>(tnext, ynext));

                    tnext += _rk2.H;
                }
            } 
            return _rk2.Points;
        }

        public List<Tuple<double, double>> GetFunctionPoints()
        {
            double x = 0;

            if (_function.Points.Count == 0)
            {
                do
                {
                    _function.Points.Add(new Tuple<double, double>(x, _f.GetY(x)));
                    x += 0.01;
                }
                while (x < _function.T);
            }

            return _function.Points;
        }

        public List<Tuple<double, double>> GetLocalErrorPoints()
        { 
            for (int i = 0; i < _rk1.Points.Count; i++)
            {
                var error = Math.Abs(((_rk2.Points[2 * i].Item2 - _rk1.Points[i].Item2) * Math.Pow(2, 4)) / (Math.Pow(2, 4) - 1));
                _error.Points.Add(new Tuple<double, double>(_rk1.Points[i].Item1, error));
            }
            return _error.Points;
        }

        public double GetGlobalError()
        {
            var error = Math.Abs(((_rk2.Points.Last().Item2 - _rk1.Points.Last().Item2) * Math.Pow(2, 4)) / (Math.Pow(2, 4) - 1));

            return error;
        }
    }
}
