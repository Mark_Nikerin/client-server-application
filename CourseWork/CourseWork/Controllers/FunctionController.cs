﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CourseWork.Main.Services;
using Microsoft.AspNetCore.Mvc;

namespace CourseWork.Controllers
{
    [Route("api/function")]
    [ApiController]
    public class FunctionController : ControllerBase
    {
        private readonly IFunctionHandlerService _functionHandlerService;

        public FunctionController(IFunctionHandlerService functionHandlerService)
        {
            _functionHandlerService = functionHandlerService;
        }

        // GET api/function
        [HttpGet]
        public ActionResult<List<Tuple<double, double>>> GetFunctionPoints()
        {
            return _functionHandlerService.GetFunctionPoints();
        }

        // GET api/function/rungeKutta1
        [HttpGet("rungeKutta1")]
        public ActionResult<List<Tuple<double, double>>> GetFirstRungeKuttaPoints()
        {
            return _functionHandlerService.GetFirstRungeKuttaPoints();
        }

        // GET api/function/rungeKutta2
        [HttpGet("rungeKutta2")]
        public ActionResult<List<Tuple<double, double>>> GetSecondRungeKuttaPoints()
        {
            return _functionHandlerService.GetSecondRungeKuttaPoints();
        }

        // GET api/function
        [HttpGet("localError")]
        public ActionResult<List<Tuple<double, double>>> GetLocalErrorPoints()
        {
            return _functionHandlerService.GetLocalErrorPoints();
        }

        // GET api/function
        [HttpGet("globalError")]
        public ActionResult<double> GetGlobalError()
        {
            return _functionHandlerService.GetGlobalError();
        }

        // POST api/function/setParams
        [HttpPost("setParams")]
        public void SetParams([FromQuery] int n, int t)
        {
            _functionHandlerService.SetParams(n, t);
        }
    }
}
