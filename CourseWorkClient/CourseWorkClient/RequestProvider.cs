﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CourseWorkClient
{
    public class RequestProvider
    {
        private readonly Uri _baseAddress;
        private readonly HttpClient _client;

        public RequestProvider(Uri baseAddress, HttpClient client)
        {
            _baseAddress = baseAddress;
            _client = client;
        }

        public async Task SetParams(int n, int t)
        {
            try
            {
                await _client.PostAsync(_baseAddress.ToString() + $"/setParams?n={n}&t={t}", null);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public async Task<HttpResponseMessage> GetFunctionPoints()
        {
            try
            {
                return await _client.GetAsync(_baseAddress.ToString());
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public async Task<HttpResponseMessage> GetFirstRungeKuttaPoints()
        {
            try
            {
                return await _client.GetAsync(_baseAddress.ToString()+"/rungeKutta1");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public async Task<HttpResponseMessage> GetSecondRungeKuttaPoints()
        {
            try
            {
                return await _client.GetAsync(_baseAddress.ToString() + "/rungeKutta2");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public async Task<HttpResponseMessage> GetLocalErrorPoints()
        {
            try
            {
                return await _client.GetAsync(_baseAddress.ToString() + "/localError");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public async Task<HttpResponseMessage> GetGlobalError()
        {
            try
            {
                return await _client.GetAsync(_baseAddress.ToString() + "/globalError");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
