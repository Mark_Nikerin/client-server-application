﻿namespace CourseWorkClient
{
    partial class CourseWorkClient
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.drawFunctionButton = new System.Windows.Forms.Button();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.drawSolutionButton = new System.Windows.Forms.Button();
            this.nParam = new System.Windows.Forms.NumericUpDown();
            this.tParam = new System.Windows.Forms.NumericUpDown();
            this.nLabel = new System.Windows.Forms.Label();
            this.tLabel = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.nParam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tParam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // drawFunctionButton
            // 
            this.drawFunctionButton.Location = new System.Drawing.Point(47, 719);
            this.drawFunctionButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.drawFunctionButton.Name = "drawFunctionButton";
            this.drawFunctionButton.Size = new System.Drawing.Size(149, 36);
            this.drawFunctionButton.TabIndex = 0;
            this.drawFunctionButton.Text = "Draw function graph";
            this.drawFunctionButton.UseVisualStyleBackColor = true;
            this.drawFunctionButton.Click += new System.EventHandler(this.drawFunctionButton_Click);
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.Location = new System.Drawing.Point(0, 0);
            this.zedGraphControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl.Size = new System.Drawing.Size(150, 150);
            this.zedGraphControl.TabIndex = 0;
            this.zedGraphControl.UseExtendedPrintDialog = true;
            // 
            // drawSolutionButton
            // 
            this.drawSolutionButton.Location = new System.Drawing.Point(268, 719);
            this.drawSolutionButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.drawSolutionButton.Name = "drawSolutionButton";
            this.drawSolutionButton.Size = new System.Drawing.Size(149, 36);
            this.drawSolutionButton.TabIndex = 1;
            this.drawSolutionButton.Text = "Draw solution graph";
            this.drawSolutionButton.UseVisualStyleBackColor = true;
            this.drawSolutionButton.Click += new System.EventHandler(this.drawSolutionButton_Click);
            // 
            // nParam
            // 
            this.nParam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nParam.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nParam.Location = new System.Drawing.Point(89, 661);
            this.nParam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nParam.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nParam.Name = "nParam";
            this.nParam.Size = new System.Drawing.Size(71, 22);
            this.nParam.TabIndex = 2;
            this.nParam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nParam.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // tParam
            // 
            this.tParam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tParam.Location = new System.Drawing.Point(223, 661);
            this.tParam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tParam.Name = "tParam";
            this.tParam.Size = new System.Drawing.Size(71, 22);
            this.tParam.TabIndex = 3;
            this.tParam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tParam.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nLabel
            // 
            this.nLabel.AutoSize = true;
            this.nLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.nLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nLabel.Location = new System.Drawing.Point(57, 663);
            this.nLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.nLabel.Name = "nLabel";
            this.nLabel.Size = new System.Drawing.Size(22, 17);
            this.nLabel.TabIndex = 4;
            this.nLabel.Text = "N:";
            // 
            // tLabel
            // 
            this.tLabel.AutoSize = true;
            this.tLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tLabel.Location = new System.Drawing.Point(191, 663);
            this.tLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tLabel.Name = "tLabel";
            this.tLabel.Size = new System.Drawing.Size(21, 17);
            this.tLabel.TabIndex = 5;
            this.tLabel.Text = "T:";
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.BackColor = System.Drawing.SystemColors.Control;
            this.errorLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(41, 786);
            this.errorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(274, 29);
            this.errorLabel.TabIndex = 6;
            this.errorLabel.Text = "Global calculation error: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CourseWorkClient.Properties.Resources.function1;
            this.pictureBox1.Location = new System.Drawing.Point(414, 608);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(227, 75);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // CourseWorkClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 985);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.tLabel);
            this.Controls.Add(this.nLabel);
            this.Controls.Add(this.tParam);
            this.Controls.Add(this.nParam);
            this.Controls.Add(this.drawSolutionButton);
            this.Controls.Add(this.drawFunctionButton);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CourseWorkClient";
            this.Text = "CourseWorkClient";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.nParam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tParam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button drawFunctionButton;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private System.Windows.Forms.Button drawSolutionButton;
        private System.Windows.Forms.NumericUpDown nParam;
        private System.Windows.Forms.NumericUpDown tParam;
        private System.Windows.Forms.Label nLabel;
        private System.Windows.Forms.Label tLabel;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

