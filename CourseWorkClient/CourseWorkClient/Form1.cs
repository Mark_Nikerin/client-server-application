﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CourseWorkClient
{
    public partial class CourseWorkClient : Form
    {
        private static Uri baseUri;
        public CourseWorkClient()
        {
            baseUri = new Uri("https://localhost:44340/api/function");
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetElements();
            Controls.Add(zedGraphControl);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            SetElements();
        }

        private void SetElements()
        { 
            zedGraphControl.Location = new Point(10, 10);
            zedGraphControl.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 100);
            zedGraphControl.GraphPane.XAxis.Scale.FontSpec.Size = 10;
            zedGraphControl.GraphPane.YAxis.Scale.FontSpec.Size = 10; 
            zedGraphControl.GraphPane.Title.Text = @"Course work. Option №15. 
Mark Nikerin, group №6314 
";

            drawFunctionButton.Location = new Point(20,
                                    zedGraphControl.Height + 40);
            drawSolutionButton.Location = new Point(drawFunctionButton.Location.X + drawFunctionButton.Width + 20,
                                   zedGraphControl.Height + 40);

            errorLabel.Hide(); 
            errorLabel.Location = new Point(drawSolutionButton.Location.X + drawSolutionButton.Width + 20, zedGraphControl.Height + 40);

            nLabel.Location = new Point(zedGraphControl.Location.X + 20, zedGraphControl.Height - 20);
            nParam.Location = new Point(nLabel.Location.X + nLabel.Width + 15, zedGraphControl.Height - 23);

            tLabel.Location = new Point(nParam.Location.X + nParam.Width + 20, zedGraphControl.Height - 20);
            tParam.Location = new Point(tLabel.Location.X + tLabel.Width + 15, zedGraphControl.Height - 23);

            pictureBox1.Location = new Point(this.Width-200, 20);
            pictureBox1.BackColor = Color.White;
        }

        private async void drawFunctionButton_Click(object sender, EventArgs e)
        {
            errorLabel.Hide();
            GraphPane pane = zedGraphControl.GraphPane;
            pane.CurveList.Clear();
            pane.Title.Text = "Function Graph";

            using (var client = new HttpClient() { BaseAddress = baseUri })
            {
                var requestProvider = new RequestProvider(baseUri, client);

                await requestProvider.SetParams((int)nParam.Value, (int)tParam.Value);

                var functionPoints = await GetFunctionPointsAsync(FunctionType.Function);

                PointPairList function = new PointPairList();

                #region LocalErrorPoints
                /*
                var localErrorPoints = await GetFunctionPointsAsync(FunctionType.LocalError);

                PointPairList localError = new PointPairList();

                foreach (var point in localErrorPoints)
                {
                    localError.Add(new PointPair(point.Item1, point.Item2));
                }
                */
                #endregion

                foreach (var point in functionPoints)
                {
                    function.Add(new PointPair(point.Item1, point.Item2));
                }

                pane.CurveList.Add(new LineItem("Function", function, Color.Red, SymbolType.None));

                pane.XAxis.Scale.Max = function.Last().X;
                pane.XAxis.Scale.Min = function.First().X;
                pane.XAxis.Title.Text = "x";
                pane.YAxis.Scale.MaxAuto = true;
                pane.YAxis.Scale.MinAuto = true;
                pane.YAxis.Title.Text = "f(x)";


                zedGraphControl.AxisChange();
                zedGraphControl.Invalidate();

            }
        } 

        private async void drawSolutionButton_Click(object sender, EventArgs e)
        {
            errorLabel.Show();
            errorLabel.Text = "Global calculation error: ";
            GraphPane pane = zedGraphControl.GraphPane;
            pane.CurveList.Clear();
            pane.Title.Text="Solution Graphs";

            using (var client = new HttpClient() { BaseAddress = baseUri })
            {
                var requestProvider = new RequestProvider(baseUri, client);

                await requestProvider.SetParams((int)nParam.Value, (int)tParam.Value);

                var firstRungeKuttaPoints = await GetFunctionPointsAsync(FunctionType.RungeKutta1);
                var secondRungeKuttaPoints = await GetFunctionPointsAsync(FunctionType.RungeKutta2);
                var globalErrorResponse = await requestProvider.GetGlobalError();

                var globalError = await globalErrorResponse.Content.ReadAsStringAsync();

                PointPairList firstRK = new PointPairList();
                PointPairList secondRK = new PointPairList();

                foreach (var point in firstRungeKuttaPoints)
                {
                    firstRK.Add(new PointPair(point.Item1, point.Item2));
                }

                foreach (var point in secondRungeKuttaPoints)
                {
                    secondRK.Add(new PointPair(point.Item1, point.Item2));
                }

                pane.CurveList.Add(new LineItem("First Solution", firstRK, Color.Blue, SymbolType.None));

                pane.CurveList.Add(new LineItem("Second Solution", secondRK, Color.Red, SymbolType.None));

                errorLabel.Text += globalError.Replace("E"," * 10^(")+")";


                pane.XAxis.Scale.Max = firstRK.Last().X;
                pane.XAxis.Scale.Min = firstRK.First().X;
                pane.XAxis.Title.Text = "t";
                pane.YAxis.Scale.Max = firstRK.Last().Y+(firstRK.Last().Y-firstRK.First().Y)*0.15;
                pane.YAxis.Scale.Min = firstRK.First().Y;
                pane.YAxis.Title.Text = "x";

                zedGraphControl.AxisChange();
                zedGraphControl.Invalidate();
            }
        }

        private async Task<List<Tuple<double, double>>> GetFunctionPointsAsync(FunctionType type)
        {
            using (var client = new HttpClient() { BaseAddress = baseUri })
            {
                var requestProvider = new RequestProvider(baseUri, client);
                switch (type)
                {
                    case FunctionType.Function:
                        {
                            var functionPointsResponse = await requestProvider.GetFunctionPoints();

                            var functionPoints = JsonConvert.DeserializeObject<List<Tuple<double, double>>>(await functionPointsResponse.Content.ReadAsStringAsync());

                            return functionPoints;
                        }
                    case FunctionType.RungeKutta1:
                        {
                            var firstRungeKuttaPointsResponse = await requestProvider.GetFirstRungeKuttaPoints();

                            var firstRungeKuttaPoints = JsonConvert.DeserializeObject<List<Tuple<double, double>>>(await firstRungeKuttaPointsResponse.Content.ReadAsStringAsync());

                            return firstRungeKuttaPoints;
                        }
                    case FunctionType.RungeKutta2:
                        {
                            var secondRungeKuttaPointsResponse = await requestProvider.GetSecondRungeKuttaPoints();

                            var secondRungeKuttaPoints = JsonConvert.DeserializeObject<List<Tuple<double, double>>>(await secondRungeKuttaPointsResponse.Content.ReadAsStringAsync());

                            return secondRungeKuttaPoints;
                        }
                    case FunctionType.LocalError:
                        {
                            var localErrorResponse = await requestProvider.GetLocalErrorPoints();

                            var localErrorPoints = JsonConvert.DeserializeObject<List<Tuple<double, double>>>(await localErrorResponse.Content.ReadAsStringAsync());

                            return localErrorPoints;
                        }
                    default: return null;
                }
            }
        }

        private enum FunctionType
        {
            Function,
            RungeKutta1,
            RungeKutta2,
            LocalError
        }
    }
}
